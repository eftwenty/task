require 'net/http'
require 'json'

class User

  def initialize username
    uri = URI.parse "https://api.github.com/users/#{username}"
    res = Net::HTTP.get uri
    @data = JSON.parse res, symbolize_names: true
  end

  def login
    @login || @data[:login]
  end

  def login= login
    @login = login.to_s.length.zero? ? nil : login.to_s
  end

  def name
    @name || @data[:name]
  end

  def name= name
    @name = name.to_s.length.zero? ? nil : name.to_s
  end

  def email
    @email || @data[:email]
  end

  def email= email
    @email = email.to_s.length.zero? ? nil : email.to_s
  end

end
